import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { listView } from '../models/list-modal';

@Injectable({
  providedIn: 'root'
})
export class WebRequestsService {
  readonly root_url

  constructor(private http: HttpClient) {
    this.root_url = 'http://localhost:3000'
  }

  get(uri: string): Observable<object> {
    return this.http.get(`${this.root_url}/${uri}`);
  };

  post(uri: string, payload: object) {
    return this.http.post(`${this.root_url}/${uri}`, payload);
  };

  patch(uri: string, payload: object) {
    return this.http.patch(`${this.root_url}/${uri}`, payload);
  };

  delete(uri: string) {
    return this.http.delete(`${this.root_url}/${uri}`);
  };

}
