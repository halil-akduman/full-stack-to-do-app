import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebRequestsService } from './web-requests.service';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {

  constructor(private webreqService: WebRequestsService) { }

  createList(title: string) {
    return this.webreqService.post('lists', { title });
  };

  getList(): Observable<any> {
    return this.webreqService.get('lists');
  }

  deleteList(id: string) {
    return this.webreqService.delete(`lists/${id}`);
  }

  getTasks(listId: string): Observable<any> {
    return this.webreqService.get(`lists/${listId}/tasks`);
  }

  createTask(title: string, listId: string) {
    return this.webreqService.post(`lists/${listId}/tasks`, { title });
  };


}
