import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NewListComponent } from './components/new-list/new-list.component';
import { TaskListComponent } from './components/task-list/task-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'ana-sayfa', pathMatch: 'full' },
  { path: 'ana-sayfa', component: HomeComponent },
  { path: 'new-list', component: NewListComponent },
  { path: 'new-list/:id/tasks', component: HomeComponent },
  { path: 'task-list', component: TaskListComponent },
  { path: '**', component: HomeComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
