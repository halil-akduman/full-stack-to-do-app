import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { listView } from 'src/app/models/list-modal';
import { taskView } from 'src/app/models/task-modal';
import { TaskServiceService } from 'src/app/services/task-service.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
    private taskService: TaskServiceService) { }
  taskList: Array<taskView> = []
  listId = ''
  ngOnInit(): void {

    this.activatedRoute.params.subscribe((params: Params) => {
      this.listId = params['id'];
      this.getTasks();
    })
  }

  createNewTask(title: string, listId: string) {
    this.taskService.createTask(title, listId).subscribe((res) => {
      this.getTasks();
      console.log('yeni kayıt olusturuldu', res);
    })
  }

  getTasks() {
    this.taskService.getTasks(this.listId).subscribe((res) => {
      this.taskList = res;
    })
  }
}
