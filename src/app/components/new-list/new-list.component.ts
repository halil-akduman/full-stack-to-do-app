import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TaskServiceService } from 'src/app/services/task-service.service';
import { listView } from 'src/app/models/list-modal';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.css']
})
export class NewListComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  name = new FormControl('');
  taskList: Array<listView> = []


  constructor(private taskService: TaskServiceService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) { }


  ngOnInit(): void {

    this.getList();


    this.form = this.fb.group(
      {
        title: [null, [Validators.required, Validators.minLength(5)]]
      }
    )
  }

  getList() {
    this.taskService.getList().subscribe(res => {
      this.taskList = res;
    });
  }

  submit() {
    this.taskService.createList(this.form.controls['title'].value).subscribe(res => {
      this.form.controls['title'].reset();
      this.getList();
    })
  }

  deleteList(id: string) {
    this.taskService.deleteList(id).subscribe((res) => {
      this.getList();
    })
  }

  goToRoute(id :any){
    this.router.navigateByUrl(`/new-list/${id}/tasks`);
  }


}
